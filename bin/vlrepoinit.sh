#!/bin/bash

#    This file is part of VectorLinux.
#
#    VectorLinux is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    VectorLinux is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    See http://www.gnu.org/licenses/ for complete licensing terms.

# Author: Moises Henriquez


# Initiate a packages repository.
# This is used to create the packages/ repository for a new release
# Should only need to run once after every release.

UNTESTED="/home/vectorlinux/Upload/vectorcontrib/build-thrower"
REPO=${REPO:-"/home/vectorlinux/Repo"}
LOGFILE="$HOME/logs/$(echo $(basename $0 .sh).log |sed 's|bin|log|g')"

function log() {
	echo "$1" >> $LOGFILE
}

function usage(){
	echo "$0 -f FILENAME.ISO.CHECKSUMS.txt -v VL_RELEASE -r REPOSITORY_TO_POPULATE"
	echo "Creates a packages/ repository from the packages defined in the"
	echo "provided -f file"
}

# 1 arg:  path to repo top
function make_metadata() {
	local myrepo=$1
        # Generate metadata
        log "Initialize metadata for $myrepo"
        (
        cd $myrepo || log "Unable to cd into $myrepo to create metadata"
        $HOME/bin/makeinfo-slack all || log "Error running makeinfo-slack on $myrepo" #>/dev/null 2>&1
        $HOME/bin/makeinfo-slapt all || log "Error running makeinfo-slapt on $myrepo" #>/dev/null 2>&1
        )
        log "Done generating metadata"

}

#2 args: (vlrelease, checksums_file
function create_packages_repo(){
	vlrelease=$1
	checksums=$2
	plist=$(basename $checksums)
	case "${plist}" in
		VL64-*)
			repotop="VL64-$(echo $vlrelease | cut -f 2 -d -)"
			distro_arch="x86_64"
			;;
		VL-*)
			repotop=$vlrelease
			distro_arch="x86"
			;;
	esac
	myrepo="$REPO/$repotop/packages"
	# Test to make sure the top dir does not exist yet.
	if [ -d $myrepo ]; then
		echo "$myrepo already exists."
		return 1
	fi
	# Process the file to extract package names and locations in the packages/ repo.
	# First test to see that all packages are present.
	$(log "INFO:  Verifying packages...")
	while read -r line
	do
		if [[ "x$line" != "x" ]]; then
			data=$(echo $line | cut -f 2-3 -d /)
			pname=$(basename $data)
			ploc=$(find $UNTESTED/$vlrelease/$distro_arch -type f -name $pname)
			if [[ "x$ploc" == "x" ]]; then
				$(log "ERROR:  Cannot find package $pname in $UNTESTED/$vlrelease/$distro_arch.")
				return 1
			fi
		fi
	done < $checksums
	$(log "INFO:  All packages verified... Lets create a repo")
	# Now that all packages are accounted for, move the actual packages.
	while read -r line
	do
		if [[ "x$line" != "x" ]]; then
			data=$(echo $line | cut -f 2-3 -d /)
			rsubdir=$(echo $data | cut -f 1 -d /)
			pname=$(basename $data)
			ploc=$(find $UNTESTED/$vlrelease/$distro_arch -type f -name $pname)
			if [[ "x$ploc" == "x" ]]; then
				log "ERROR:  Cannot find $pname in $UNTESTED/$vlrelease/$distro_arch"
				return 1
			fi
			if [ ! -d $myrepo/$rsubdir ]; then
				log "DEBUG: Create directory $myrepo/$rsubdir"
			fi
			log "DEBUG: Move package $pname ->  $myrepo/$rsubdir"
			# This should be fixed @ the ISO, but just in case.
			if [[ "$(pkgname $pname)" == kernel* ]]; then
				rsubdir="kernels"
			fi
			mkdir -p $myrepo/$rsubdir >/dev/null 2>&1
			cp $ploc $myrepo/$rsubdir
		fi
	done < $checksums
	# Generate metadata
	make_metadata $myrepo
#	log "Initialize metadata for $myrepo/$rsubdir"
#	(
#	cd $myrepo
#	$HOME/bin/makeinfo-slack all >/dev/null 2>&1
#	$HOME/bin/makeinfo-slapt all >/dev/null 2>&1
#	)
#	log "Metadata has been generated."
}

function create_extra_repo() {
	vlrelease=$1
	checksums=$2
	ignorelist=$(basename $checksums)
	# Everything that is in checksums gets ignored.  Everything else goes into extra.

	case "${ignorelist}" in
		VL64-*)
			repotop="VL64-$(echo $vlrelease|cut -f 2 -d -)"
			distro_arch="x86_64"
			;;
		VL-*)
			repotop=$vlrelease
			distro_arch="x86"
			;;
	esac

	myrepo="$REPO/$repotop/extra"
	#XX: Warn if this already exists, and exit to be safe.
	if [ -d $myrepo ]; then
		echo "$myrepo already exists."
		return 1
		#rm -rf $myrepo
	fi
	mkdir -p $myrepo
	# Ignore verifying packages, because this is extra stuff... not packages/
	for pkg in $(find $UNTESTED/$vlrelease/$distro_arch -type f -name *.t?z); do
		pfname=$(basename $pkg)
		pname=$(pkgname $pfname)
		pver=$(pkgversion $pfname)
		ign=$(grep $pname $checksums)
		if [[ "x$ign" != "x" ]]; then
			msg="[ IGNORE ] $pkg "
			log "$msg"
			continue
		else
			# Use the vlrepomaint.sh tool to move the package
			msg="[ MOVE ] $pname to extra"
			log "$msg"
#			ploc=$(guess_pkg_location $pkg)
			ploc=$(find_pkg_loc $vlrelease $pkg)
			echo "$pkg % $ploc"  >> $HOME/logs/results.log
			tdir=$myrepo/$ploc # Target file name
			mkdir -p $tdir >/dev/null 2>&1
			cp $pkg $tdir/
		fi
	done
	# Now generate the metadata
	make_metadata $myrepo
#	(
#		cd $myrepo
#		log "Generating slack metadata..."
#		$HOME/bin/makeinfo-slack all >/dev/null 2>&1
#		log "Generating slapt-get metadata ..."
#		$HOME/bin/makeinfo-slapt all >/dev/null 2>&1
#	)
#
}


while getopts "v:f:r:h" opt; do
        case $opt in
                v) vlrelease=${OPTARG};;
                f) checksums=${OPTARG};;
		r) make_repo=${OPTARG};;
                h) usage; exit 0;;
                *) echo "Unsupported option $opt"; exit 1 ;;
        esac
done
source $HOME/bin/vlrepomaint.sh || exit 1 #{ "Unable to source vlrepomaint.sh tool"; exit 1}
case $make_repo in
	packages)
		echo "Logging to $LOGFILE"
		$(log "DEBUG: =========== $(date) Create $vlrelease stable repository from $checksums ============")
		create_packages_repo $vlrelease $checksums
		$(log "DEBUG: =========== $(date) Finished creating $vlrelease stable repository from $checksums =========")
		;;
	extra)
		echo "Populating extra/ repository for $vlrelease"
		source $HOME/bin/vlrepomaint.sh >/dev/null 2>&1
		create_extra_repo $vlrelease $checksums
		;;
	*)
		echo "This tool can only be used to populate the packages/ and extra/ repositories."
		exit 1
		
esac
