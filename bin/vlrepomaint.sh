#!/bin/bash

#    This file is part of VectorLinux.
#
#    VectorLinux is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    VectorLinux is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    See http://www.gnu.org/licenses/ for complete licensing terms.

# Author: Moises Henriquez


UNTESTED="/home/vectorlinux/Upload/vectorcontrib/build-thrower"
REPO=${REPO:-"/home/vectorlinux/Repo"}
LOGFILE=$HOME/logs/vlrepomaint.log

function LOG() {
	message="$1"
	echo "$(date '+%D %T') $message" >> $LOGFILE
	}

# 3 args: (vlrelease, pkg_src_path)
function find_pkg_loc() {
# Determine where the package belongs in the stable repos.  
# If no current copy is found, libUntestedPkg will guess where it goes.
	vlrelease=$1
	pkg_srcloc=$2
	pkg=$(pkgname $(basename $pkg_srcloc))
	ret=""
	# look in packages, extra, patches.
	for repo in packages exra patches; do
		if [ ! -d $REPO/${vlrelease}/${repo} ]; then
			continue
		fi
		ploc=$(find $REPO/${vlrelease}/${repo} -type f -name ${pkg}-*.t?z)
		for p in $ploc; do
			if [[ $(pkgname $(basename $p)) == "$pkg" ]]; then
				tpkg="$p"
				break
			fi
		done
		if [ "x$tpkg" != "x" ]; then
			# found a hit
#			echo "Found a hit: $ploc"
			prop="$(echo "$tpkg" | sed "s|$REPO/$vlrelease/$repo||g")"
			prop=$(basename $(dirname $prop))
			ret="$prop"
			break
		fi
		if [[ "x$ret" == "x" ]]; then
			# bring out the big guns... this package is new to the repositories.
			# Lets determine using libUntestedPkg
			#ret="$($HOME/bin/libUntestedPkg -p ${pkg})"
			ret="$($HOME/bin/libUntestedPkg -p $pkg_srcloc)"
		fi
	done
	echo "$ret"
}

# 2 args: (list_of_words, match)
function OneOf(){
	wlist="$1"
	match="$2"
	ret="NO"
	for i in $(echo $wlist); do
		if [[ "$i" == "$match" ]]; then
			ret="YES"
			break
		fi
	done
	echo $ret

}
#2 args: (vlrelease, vl_arch)
function get_osl_repo_dir() {
	vlrelease=$1
	vl_arch=$2
	ret=""
	case $vl_arch in
		x86|i[3456]86)
		ret=$vlrelease
		;;
		x86_64|amd64)
		ret="VL64-$(echo $vlrelease | cut -f 2 -d -)"
		;;
		*)
		echo "Unknown Vector architecture." >&2
		exit 1
	esac
	echo $ret
}

# 3 args: (vlrelease, vl_arch, repo_name)
function get_repo_topdir() {
	vlrelease=$1
	vl_arch=$2
	wanted_repo=$3
	repo_path=""
	case $wanted_repo in
		untested)
			case $vl_arch in
				i[3456]86|x86)
					repo_path=$UNTESTED/$vlrelease/x86
					;;
				x86_64|amd64)
					repo_path=$UNTESTED/$vlrelease/x86_64
					;;
				esac
			;;
		packages|extra|patches|testing)
			repodir=$(get_osl_repo_dir $vlrelease $vl_arch)
			repo_path=$REPO/$repodir/$wanted_repo
			;;
		*)
			echo "Uknown repository $wanted_repo in $vlrelease" >&2
			return 1
			;;
	esac
	echo $repo_path
}

# 4 args: (vlrelease, source_repo, target_repo, pkg_name)
function MovePkg() {
	vlrelease=$1
	source_repo=$2
	target_repo=$3
	pkg_name=$4

	$(LOG "[ $vlrelease ] Moving package $pkg_name from $source_repo to $target_repo")
	case $source_repo in
		untested)
			pkgpath32="$(find $UNTESTED/$vlrelease/x86 -type f -name $pkg_name-*.t?z)"
			pkgpath64="$(find $UNTESTED/$vlrelease/x86_64 -type f -name $pkg_name-*.t?z)"
			;;
		testing|extra)
			# 32-bit and 64-bit should.  This is just to determine the location.
			pkgpath32="$(find $REPO/$vlrelease/$source_repo -type f -name $pkg_name-*.t?z)"
			pkgpath64="$(find $REPO/VL64-$(echo $vlrelease | cut -f 2 -d -)/$source_repo -type f -name $pkg_name-*.t?z)"
			;;
		packages|patches)
			echo "Packages should *NEVER* be moved out of the $source_repo area" >&2
			return 1
			;;
		*)
			msg="Unknown $source_repo repository"
			echo "$msg" >&2
			$(LOG $msg)
			return 1
			;;
	esac
	if [[ "x$pkgpath32" == "x" ]] || [[ "x$pkgpath64" == "x" ]]; then
		err="No package $pkg_name found"
		$(LOG "$err")
		echo "$err" >&2
		return 1
	elif [[ $(echo $pkgpath | wc -w) -gt 1 ]]; then
		msg="WARNING:  Multiple packages found while searching for $pkg_name.  They WILL ALL BE MOVED"
		echo "$msg" >&2
		$(LOG "$msg")
	fi

	#XX:  Move 32-bit package
	for pkg in $pkgpath32; do
		tgt32=$(get_osl_repo_dir $vlrelease "i586")
		tloc=$(find_pkg_loc $vlrelease $pkg)
		targetdir=$REPO/$tgt32/$target_repo/$tloc
		mfrom="$vlrelease/$source_repo/$(basename $(dirname $pkg))/$(basename $pkg)"
		msg="[ MOVE ] $mfrom -> $targetdir"
		echo "$msg" >&2
		$(LOG "$msg")
		mkdir -p $targetdir >/dev/null 2>&1
		mv -v $pkg $targetdir
	done

	#XX: Move 64-bit package
	for pkg in $pkgpath64; do
		tgt64=$(get_osl_repo_dir $vlrelease "x86_64")
		tloc=$(find_pkg_loc $vlrelease $pkg)
		#XX: ^^ We could use the same as 32-bit, but meh
		targetdir=$REPO/$tgt64/$target_repo/$tloc
		mfrom="$vlrelease/$source_repo/$(basename $(dirname $pkg))/$(basename $pkg)"
		msg="[ MOVE ] $mfrom -> $targetdir"
		echo "$msg" >&2
		$(LOG "$msg")
		mkdir -p $targetdir >/dev/null 2>&1
		mv -v $pkg $targetdir
	done
}

function usage() {
	echo "Move packages between repositories"
	echo "$0 -s source_repo -t target_repo -v vl_release -p pkg"
}

#3 args (vlrelease, source_repo, target_repo)
function move_all_packages() {
	vlrelease=$1
	src_repo=$2
	tgt_repo=$3

	for vl_arch in x86; do
		mv_from=$(get_repo_topdir $vlrelease $vl_arch $src_repo)
		if [ ! -d $mv_from ]; then
			$(LOG "Unable to find $mv_from directory")
			echo "Unable to find $mv_from " >&2
			return 1
		fi
		$(LOG "Moving ALL packages from $mv_from to $tgt_repo")
		for pkg in $(find $mv_from -type f -name *.t?z); do
			pname=$(pkgname $(basename $pkg))
			MovePkg $vlrelease $src_repo $tgt_repo $pname || return 1
			if [ $? -gt 0 ]; then
				$(LOG "ERROR:  moving $pname from $source_repo to $target_repo failed!.")
			fi
		done

	done

}

# 2 arg: (vlrelease, repository to finalize (update metadata)
function finalize() {
	vlrelease=$1
	repo=$2
	if [[ "$FINALIZE" != "TRUE" ]]; then
		return 0 # No need to run
	fi
	for vlarch in x86 x86_64; do
		topdir=$REPO/$(get_osl_repo_dir $vlrelease $vlarch)/$repo
		LOG "Updating metadata for $topdir"
		cd $topdir
		# run makeinfo
		$HOME/bin/makeinfo-slack new
		$HOME/bin/makeinfo-slapt new t[glbx]z
		LOG "Finished updating metadata"
	done

}

while getopts "v:t:p:s:hyF" opt; do
	case $opt in
		F) FINALIZE="TRUE";;
		v) vlrelease=${OPTARG};;
		t) TARGET_REPO=${OPTARG};;
		s) SOURCE_REPO=${OPTARG};;
		p) pkg=${OPTARG};;
		y) NONSTOP="TRUE";;
		h) usage; exit 0;;
		*) echo "Unsupported option $opt"; exit 1 ;;
	esac
done



if [[ "x$pkg" == "x" ]]; then
	# assume we have been sourced.
	echo "Missing package path" >&2
else
	case "$pkg" in
		all)
			# Be really careful here
			$(LOG "Move ALL packages from $SOURCE_REPO to $TARGET_REPO")
			move_all_packages $vlrelease $SOURCE_REPO $TARGET_REPO
			;;
		*)
			if [[ "$SOURCE_REPO" == "$TARGET_REPO" ]]; then
				echo "Source and Target repositories are the same... Nothing to do here" >&2
				exit 1
			fi
			$(LOG "Move $pkg from $SOURCE_REPO to $TARGET_REPO")
			finalize $vlrelease $TARGET_REPO
			exit 0
			MovePkg $vlrelease $SOURCE_REPO $TARGET_REPO $pkg
			;;
	esac
fi
