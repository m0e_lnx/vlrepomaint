#!/bin/bash

#    This file is part of VectorLinux.
#
#    VectorLinux is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License v3 as published by
#    the Free Software Foundation.
#
#    VectorLinux is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    See http://www.gnu.org/licenses/ for complete licensing terms.

# Author: Moises Henriquez

LOGFILE=$HOME/logs/vlRepoUtil.log
FAILLOG=$HOME/logs/vlRepoUtil.failures.log
STABLETOP=$HOME/Repo
UNTESTEDTOP=$HOME/Upload/vectorcontrib/build-thrower

function LOG() {
	messge="$1"
	echo "$(date '+%D %T') $messge" >> $LOGFILE
	}
function FLOG() {
    message="$1"
    echo "$(date '+%D %T') $message" >> $FAILLOG
}

# 3 args (vlrelease, arch, repo-name)
function get_path_to_repo() {
	local vlrelease=$1
	local vlarch=$2
	local repo=$3

	case $vlarch in
		i[3456]86|x86)
			local osl_repo_subdir=$vlrelease
			local untested_subdir="x86"
			;;
		x86_64|amd64)
			local subdir=$(echo $vlrelease|cut -f 2 -d -)
			local osl_repo_subdir="VL64-${subdir}"
			local untested_subdir="x86_64"
			;;
		*)
			LOG "Uknown architecture $vlarch.  get_path_to_repo failed!."
			return 1
			;;
	esac

	case $repo in
		untested)
			local retpath="$UNTESTEDTOP/$vlrelease/$untested_subdir"
			;;
		extra|packages|patches|testing)
			local retpath="$STABLETOP/$osl_repo_subdir/$repo"
			;;
		*)
			LOG "Unknown repository for $vlrelease.  get_path_to_repo failed!."
			return 1
			;;
	esac
	echo "$retpath"
}

# 3 args(pkgname, pkgarch, vlrelease, repo_name)
# ^^ Prints the absolute path to a package in the given location
#XX: find_pk_in_repo htop i586 veclinux-7.1 extra
#XX: << /home/vectorlinux/Repo/VL64-7.1/extra/xap/System/htop-1.0.2-x86_64-1vl71.txz
function find_package_in_repo() {
	local pkg=$1
	local pkgarch=$2
	local vlrelease=$3
	local find_in_repo=$4
	local repopath="$(get_path_to_repo $vlrelease $pkgarch $find_in_repo)"
	local retpath=""
	if [[ "x$repopath" = "x" ]]; then
		LOG "$FUNCNAME: Failed to get path to repository $find_in_repo"
		return 1
	fi
	local results=$(find $repopath -type f -name $pkg-*.t?z)
	for p in $results; do
		pname=$(pkgname $(basename $p))
		if [[ "$pname" == "$pkg" ]]; then
			# Found a match
			LOG "$FUNCNAME: Found package $pkg in $p"
			retpath=$p
			echo "$retpath"
			return 0
			break
		fi
	done

	return 1
}

# 3 args: (pkgname, vlrelease, pkgarch)
#XX: locate_pkg_in_stable htop veclinux-7.1 x86_64
function locate_pkg_in_stable() {
	local pkg=$1
	local vlrelease=$2
	local pkgarch=$3
	local retval=""
	local repomatch=""
	# Get path to stable repo
	for loc in patches extra testing; do #XX: <-- Dont look in packages?
	    if [[ "x$retval" != "x" ]]; then
	        break
	    fi
	    hits="$(find_package_in_repo $pkg $pkgarch $vlrelease $loc)"
	    for p in $hits; do
	    	pname="$(pkgname $(basename $p))"
		repomatch=$(get_path_to_repo $vlrelease $pkgarch $loc)
		if [[ "$pname" == "$pkg" ]]; then
		    LOG "$FUNCNAME: Located package $pkg in stable repos as $p"
		    retval="$p"
		    break
		fi
	    done
	done
	# If we dont have a result yet, we have trouble
	if [[ "x$retval" == "x" ]]; then
	    LOG "$FUNCNAME: Unable to locate package $pkg in stable repositories for $vlrelease $pkgarch"
	    return 1
	fi
	# At this point we should have an absolute path to the package
	# Need to process it to get only the relative path.
	retval=$(dirname "$retval")
	retval=$(echo $retval | sed "s|$repomatch||g")
	retval=$(echo $retval | cut -f 2-3 -d /)
	echo "$retval"


}

# 1 arg: absolute path to package
function guess_pkg_location() {
    local pkgpath="$1"
    retval="$($HOME/bin/libUntestedPkg -p $pkgpath)"
    if [[ "x$retval" == "x" ]]; then
        LOG "FUNCNAME: Unable to determine a suitable location for $pkgpath"
	return 1
    fi
    LOG "$FUNCNAME: Found suitable location for $pkgpath '$retval'"
    echo "$retval"

}

function kept_package() {
# 1 argument: package name.
#XX: Returns "YES" if older versions of package $pname should be kept
#XX: when a new version is introduced.
    local safelist="kernel kernel-modules kernel-src kernel-src-stripped ndiswrapper bcm_wimax broadcom-sta amd-catalyst-driver"
    local pname=$1
    local retval="NO"

    for item in $safelist; do
	if [[ "$item" == "$pname" ]]; then
	    retval="YES"
	    break
	fi
    done
    echo "$retval"
}

# 4 args: (vlrelease, pkgname, move_from, move_to)
function move_package() {
    local vlrelease=$1
    local pname=$2
    local pfrom=$3
    local pto=$4
    local rmlist=""
    #XX: Validate where packages are moved from ( we dont want 'packages' here)
    #XX: Validate where packages are moved to (we dont want 'packages' here either)

    # First find the package in the source location
    # We will cycle through the supported architectures
    LOG "$FUNCNAME: REQUEST: Move $pname for $vlrelease from $pfrom to $pto"
    for parch in x86 x86_64; do
    	pfile=$(find_package_in_repo $pname $parch $vlrelease $pfrom)
	if [[ "x$pfile" != "x" ]]; then
	    LOG "$FUNCNAME: Found $parch $pname package: $pfile"
	    #XX: Find out where this package belongs in the target repo.
	    if [[ "$parch" == "x86" ]]; then
	    	p32=$pfile
	    elif [[ "$parch" == "x86_64" ]]; then
	    	p64=$pfile
	    fi
	    in_repo_loc="$(locate_pkg_in_stable $pname $vlrelease $parch)"
	    if [[ "x$in_repo_loc" != "x" ]]; then
	    	LOG "$FUNCNAME: MATCHED: Matched location of $pname '$in_repo_loc'"
		if [[ "$(kept_package $pname)" != "YES" ]]; then
		    rmlist+="$(find_package_in_repo $pname $parch $vlrelease $pto) "
		else
		    LOG "$FUNCNAME: KEPT_OLD_PACKAGE: Older versions of package $pname will not ve removed."
		fi

	    else
	        LOG "$FUNCNAME: Unable to match location for $pname... Trying to guess it."
		in_repo_loc="$(guess_pkg_location $pfile)"
	    fi
	    if [[ "x$in_repo_loc" == "x" ]]; then
	    	LOG "$FUNCNAME: STOP! Unable to determine where $pname belongs in $pto for $vlrelease"
		return 1
	    fi
	else
	   LOG "$FUNCNAME: FAILURE: Unable to find $parch package $pname in $pfrom"
	   FLOG "$vlrelease:$pname:$pfrom:$pto:$parch"
	   return 1
	fi
	local proposed_loc="$(get_path_to_repo $vlrelease $parch $pto)"
	if [[ "x$proposed_loc" == "x" ]]; then
	    LOG "$FUNCNAME: FAILURE: Unable to determine a suitable location for $pfile in the stable repositories"
	    FLOG "$vlrelease:$pname:$pfrom:$pto:$parch"
	    return 1
	fi
	LOG "$FUNCNAME: RESULT: Proposed target location for $pname: $proposed_loc/$in_repo_loc"
	if [[ "$parch" == "x86" ]]; then
	    target32=$proposed_loc/$in_repo_loc
	elif [[ "$parch" == "x86_64" ]]; then
	    target64=$proposed_loc/$in_repo_loc
	fi
    done

    # Now that we have p32, p63, target32 and target42, we move.
    #rmtarget32="$(echo $rmtarget32 | sed 's|.t[lbgx]z|.*|g')"
    #rmtarget64="$(echo $rmtarget64 | sed 's|.t[lbgx]z|.*|g')"
    LOG "$FUNCNAME: TO_MOVE: $p32:$target32"
    LOG "$FUNCNAME: TO_MOVE: $p64:$target64"
    for rmtarget in $rmlist; do
    	rmline="$(echo $rmtarget | sed 's|.t[glbx]z|.*|g')"
        LOG "$FUNCNAME: DO_DELETE: $rmline"
	#XX: Remove the old package and its .info and .meta file
	rm -rf $rmline >/dev/null 2>&1
    done
    #XX: Make sure the target dir exists
    mkdir -p $target32 >/dev/null 2>&1
    mkdir -p $target64 >/dev/null 2>&1
    cp $p32 $target32/ >/dev/null 2>&1
    cp $p64 $target64/ >/dev/null 2>&1


}

#1 arg: string
function is_trash() {
    local datain="$1"
    if [[ "x$datain" == "x" ]]; then
#        LOG "$FUNCNAME: TASKLIST: Empty line"
	return 1
    elif [[ "$datain" =~ ^#.* ]]; then
#        LOG "$FUNCNAME: TASKLIST: Commented line \"$datain\""
        return 1
    elif [[ $(echo "$datain" |grep -o ":"|wc -l) -lt 3 ]]; then
       LOG "$FUNCNAME: TASKLIST: Malformed line \"$datain\""
       return 1
    fi
    return 0

}
# 1 arg:  absolute path to file defining the moves need
function process_tasklist() {
    local fname=$1
    local affected_repos=""
    if [ ! -e $fname ]; then
    	LOG "$FUNCNAME: TASKLIST: Specified tasklist file $fname does not exist."
	return 1
    fi
    local newmd5sum=$(md5sum $fname | cut -f 1 -d ' ')
    local tmpbackup=$HOME/tmp/repotool.tasklist.txt
    local oldmd5sum=$(md5sum $tmpbackup| cut -f 1 -d ' ')
    if [[ "$oldmd5sum" == "$newmd5sum" ]]; then
    	LOG "$FUNCNAME: TASKLIST: No changes in task list... Nothing to do"
	return 0
    fi

    #XX: Store a copy of the last changed file:
    cp $fname $tmpbackup
    #XX: process the file
    while read -r line
        do
	    is_trash "$line" || continue
   	    local vlrelease=$(echo "$line" | cut -f 1 -d:)
	    local pname=$(echo "$line" | cut -f 2 -d :)
	    local mvFrom=$(echo "$line" | cut -f 3 -d :)
	    local mvTo=$(echo "$line" | cut -f 4 -d :)
	    LOG "$FUNCNAME: TASKLIST: $vlrelease: MOVE $pname FROM $mvFrom TO $mvTo"
	    move_package $vlrelease $pname $mvFrom $mvTo
	    if [[ $? -gt 0 ]]; then
	    	LOG "$FUNCNAME: TASKLIST: FAILED to perform \"$line\""
		return 1
	    fi
	    if [[ "$affected_repos" != $vlrelease:$mvTo* ]]; then
		affected_repos+="$vlrelease:$mvTo "
	    fi
    done < $fname
    #XX: Update the metadata as needed for the affected repos where
    #XX: packages where added/changed
    for line in $affected_repos; do
	rvlrelease=$(echo $line | cut -f 1 -d :)
	repo=$(echo $line | cut -f 2 -d :)
	path32="$(get_path_to_repo $rvlrelease x86 $repo)"
	path64="$(get_path_to_repo $rvlrelease x86_64 $repo)"
	( cd $path32
	LOG "$FUNCNAME: METADATA: Refreshing metadata for $path32"
	$HOME/bin/makeinfo-slack new
	$HOME/bin/makeinfo-slapt new t[glbx]z
	)
	( cd $path64
	LOG "$FUNCNAME: METADATA: Refreshing metadata for $path64"
	$HOME/bin/makeinfo-slack new
	$HOME/bin/makeinfo-slapt new t[glbx]z
	)
    done

}

function usage() {
    cat <<END

$0 [-t tasklist_file] | [-F ] -p package_name -v vector_release -s moving_from -t moving_to

This tool can operate in (2) modes.
MODE 1: Using a task list.

OPTIONS:
    -t : Processes the tasklist in the provided file (absolute path needed)
         Re-generates the repository metadata after processing the tasklist

MODE 2: Using flags to indicate which package to move and where it goes.
OPTIONS:
    -p : package_name
    -v : vector_release
    -s : Move package From
    -t : Move package to
    -F : Re-generate metadata after moving package.

END
}

if [ $# -eq 0 ]; then
   # Assume we have been sourced.
   echo "Sourced" >/dev/null
elif [[ "$1" == "-t" ]]; then
    process_tasklist $2
else
#    echo "When not using the tasklist, you need a -p, -v, -s and -t flags"
#    echo "See $0 -h for more information"
#fi

while getopts "p:v:s:t:hF" opt; do
    case $opt in
        f) tasklist=${OPTARG};;
	p) pname=${OPTARG};;
	v) vlrelease=${OPTARG} ;;
	s) mvFrom=${OPTARG};;
	t) mvTo=${OPTARG};;
	F) do_regenerate_meta="TRUE";;
	h) usage; exit 0;;
	*) echo "Unsupported option $opt"; exit 1;;
    esac
done
fi
